```java
import java.util.Scanner;

import javax.sound.sampled.SourceDataLine;

import java.text.DecimalFormat;
class Player {
    private String nama;
    private int level;
    private int menang;
    private int kalah;
    private double total;
    private double winrate;

    
    DecimalFormat decimalFormat = new DecimalFormat("#.##");

    public Player(){
        this.nama = nama;
        this.level = level;
        menang = 0;
        kalah = 0;
        

    }

    public void setMatch(){
        Random random = new Random();
        int randomNomor = random.nextInt(2); //menghasilkan nilai acak dari 0 hingga 1
        if (randomNomor == 0){
            System.out.println("=== VICTORY!!! ===");
            menang++;
        } else {
            System.out.println("=== DEFEAT!!! ===");
            kalah++;

        }
        total = menang + kalah;
        winrate = menang/total*100;
        
}

    public String getNama(){
        return nama;
    }

    public void setNama(String nama){
        this.nama = nama;
    }

    public int getLevel(){
        return level;
    }

    public void setLevel(int level){
        this.level = level;
    }

    public int getMenang(){
        return menang;
    }


    public int getKalah(){
        return kalah;
    }

    public double getTotal(){
        return total;
    }


    public double getWinrate(){
        if(total == 0){
            return 0;
        }
        return (double) menang/total*100;
        
    }
    

    public void profile(){
        System.out.println("Username \t\t: " + this.nama);
        System.out.println("Level \t\t\t: " + this.level);
        System.out.println("Total Pertandingan \t: " + getTotal());
        System.out.println("Jumlah Kemenangan \t: " + getMenang());
        System.out.println("Jumlah Kekalahan \t: " + getKalah());
        System.out.println("WINRATE : " + decimalFormat.format(getWinrate()) + " %");
    }
}

abstract class GameML{
    abstract void showpilih(); 
}

class pilIngem extends GameML{
    void showpilih(){
        System.out.println("0. Kalahkan Minion");
                        System.out.println("2. Poking damage Ke musuh");
                        System.out.println("3. Kalahkan kepiting");
                        System.out.println("4. Bergerak Ke Midlane");
                        System.out.println("5. Ambush");
                        System.out.println("6. Cutting Lane");
                        System.out.println("7. Hancurkan Turret");
                        System.out.println("8. Hasil Pertandingan");
                        System.out.print("Pilihan: ");
    }
}


class pilrole {
    void showpilih(){
            Scanner scanner = new Scanner (System.in);
            Hero[] roles = new Hero[6];
            roles[0] = new Assassin ();
            roles[1] = new Fighter();
            roles[2] = new Marksman();
            roles[3] = new Tank();
            roles[4] = new Support();
            roles[5] = new Mage();
        
            System.out.println("ROLE LIST");
            for (int i = 0; i < roles.length; i++){
                System.out.println((i+1) + ". " + roles[i].getNama() + " - " + roles[i].getDeskripsi());
        
            }
        
            System.out.print("Masukkan Role yang anda pilih: ");
            int rolepilihan = scanner.nextInt();
            if(rolepilihan > 0 && rolepilihan <= roles.length){
                Hero pilihRole = roles[rolepilihan - 1];
                if(pilihRole instanceof Tank ){
                    System.out.println("HERO LIST");
                    Tank tankRole = (Tank) pilihRole;
                    String[] heroList = tankRole.getHeroList();
                    for (int i = 0; i < heroList.length; i++){
                        System.out.println((i + 1) + ". " + heroList[i]);
                    }
                }else if (pilihRole instanceof Marksman) {
                    System.out.println("HERO LIST");
                    Marksman marksmanRole = (Marksman) pilihRole;
                    String [] heroList = marksmanRole.getHeroList();
                    for (int i = 0; i < heroList.length; i++){
                        System.out.println((i + 1) + ". " + heroList[i]);
                    }
                }else if(pilihRole instanceof Assassin){
                    System.out.println("HERO LIST");
                    Assassin assasinRole = (Assassin) pilihRole;
                    String [] heroList = assasinRole.getHeroList();
                    for (int i = 0; i < heroList.length; i++){
                        System.out.println((i + 1) + ". " + heroList[i]);
                    }
                }else if(pilihRole instanceof Support){
                    System.out.println("HERO LIST");
                    Support supportRole = (Support) pilihRole;
                    String [] heroList = supportRole.getHeroList();
                    for (int i = 0; i < heroList.length; i++){
                        System.out.println((i + 1) + ". " + heroList[i]);
                    }
                }else if(pilihRole instanceof Fighter){
                    System.out.println("HERO LIST");
                    Fighter fighterRole = (Fighter) pilihRole;
                    String [] heroList = fighterRole.getHeroList();
                    for (int i = 0; i < heroList.length; i++){
                        System.out.println((i + 1) + ". " + heroList[i]);
                    }
                }else if(pilihRole instanceof Mage){
                    System.out.println("HERO LIST");
                    Mage mageRole = (Mage) pilihRole;
                    String [] heroList = mageRole.getHeroList();
                    for (int i = 0; i < heroList.length; i++){
                        System.out.println((i + 1) + ". " + heroList[i]);
                    }
                }else{
                    System.out.println("tidak ada hero");
                }
            }else {
                System.out.println("Tidak Valid");
            }
        }
    }



abstract class Hero {
    private String nama;
    private String deskripsi;

    public Hero(String nama, String deskripsi) {
        this.nama = nama;
        this.deskripsi = deskripsi;
    }

    public String getNama(){
        return nama;
    }

    public String getDeskripsi(){
        return deskripsi;
    }

}
class Assassin extends Hero {
    private String [] heroList;

    public Assassin() {
        super("Assassin", "Mampu menghasilkan damage yang tinggi dalam waktu singkat, tetapi memiliki HP yang rendah.");
        heroList = new String[] {"Fanny", "Helcurt", "Aamon", "Saber", "Hayabusa", "Ling", "Lancelot"};
    }

    public String[] getHeroList(){
        return heroList;
    }
}
class Mage extends Hero {
    private String [] heroList;

    public Mage() {
        super("Assassin", "Mampu menghasilkan damage yang tinggi dalam waktu singkat, tetapi memiliki HP yang rendah.");
        heroList = new String[] {"Fanny", "Helcurt", "Aamon", "Saber", "Hayabusa", "Ling", "Lancelot"};
    }

    public String[] getHeroList(){
        return heroList;
    }
}

class Tank extends Hero {
    private String [] heroList; 
    public Tank() {
        super("Tank", "Mampu menahan damage dan menarik perhatian musuh, tetapi memiliki damage yang rendah.");
        heroList = new String[] {"Hylos", "Atlas", "Khufra", "Lolita", "Franco", "Akai", "Tigreal"};
    }

    public String[] getHeroList(){
        return heroList;
    }
}

class Support extends Hero {
    private String [] heroList;
    public Support() {
        super("Support", "Mampu memberikan support pada hero lain dan mengganggu musuh.");
        heroList = new String[] {"Angela", "Floryn", "Mathilda", "Carmila", "Estes"};
    }
    public String[] getHeroList(){
        return heroList;
    }
}

class Marksman extends Hero {
    private String[] heroList;

    public Marksman() {
        super("Marksman", "Mampu menembak dari jarak jauh dan menghasilkan damage yang tinggi, tetapi memiliki HP yang rendah.");
        heroList = new String[] {"Miya", "Claude", "Bruno", "Moskov", "Layla"};
    }
    public String[] getHeroList() {
        return heroList;
    }
    }

    class Fighter extends Hero {
        private String[] heroList;
        
        public Fighter(){
            super("Fighter", "Hero yang mempunyai darah tebal dan damage cukup tinggi");
            heroList = new String [] {"Lapu-lapu", "Yuzhong", "Chou", "Terizla", "Thamuz"};
        }

        public String [] getHeroList(){
            return heroList;
        }


}



public class main {
    public static void main(String[] args) {


        pilrole pilih = new pilrole();
        Scanner scanner = new Scanner(System.in);
        Player player = new Player();

        System.out.println("=== SELAMAT DATANG DI MOBILE LEGENDS ===");
        System.out.print("Masukkan Username : ");
        String nama = scanner.nextLine();
        System.out.print("Password : ");
        String password = scanner.nextLine();
        System.out.print("Masukkan Level : ");
        int level = scanner.nextInt();

        player.setNama(nama);
        player.setLevel(level);

        if (nama.equals(nama) && password.equals("123")){
            System.out.println("Login Berhasil");
            int pil1;
            do {
                System.out.println("\n=== SELAMAT DATANG DI MOBILE LEGENDS ===");
                System.out.println("1. Profile");
                System.out.println("2. Mode Rank");
                System.out.println("3. Logout");
                System.out.print("Pilihan : ");
                pil1 = scanner.nextInt();

                switch(pil1){
                    case 1:
                        //memanggil menu profil
                        player.profile();
                    break; 
                    case 2:
                    //memanggil CLI untuk mode rank(pilih role)
                    pilih.showpilih();
                    break;
                    case 3:
                    //Logout(kembali ke menu insert username & password)
                    break;
                    default:

                    break;
                }

            }while(pil1!=4);
        }else{
            System.out.println("Logiin Gagal!!");
        }
        
    }
}
```
