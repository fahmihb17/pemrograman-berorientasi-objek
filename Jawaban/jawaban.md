# No 1.
- **Use Case** : User dapat memilih hero

```java
    void pilihHero(){
        Scanner scanner = new Scanner (System.in);
        Hero[] roles = new Hero[6];
        roles[0] = new Assassin ();
        roles[1] = new Fighter();
        roles[2] = new Marksman();
        roles[3] = new Tank();
        roles[4] = new Support();
        roles[5] = new Mage();

        System.out.println("\nROLE LIST");
        for (int i = 0; i < roles.length; i++){
            System.out.println((i+1) + ". " + roles[i].getNama() + " - " + roles[i].getDeskripsi());

        }

        System.out.print("\nMasukkan Role yang anda pilih: ");
        int rolepilihan = scanner.nextInt();
        if(rolepilihan > 0 && rolepilihan <= roles.length){
            Hero pilihRole = roles[rolepilihan - 1];
            if(pilihRole instanceof Tank ){
                System.out.println("HERO LIST");
                Tank tankRole = (Tank) pilihRole;
                String[] heroList = tankRole.getHeroList();
                for (int i = 0; i < heroList.length; i++){
                    System.out.println((i + 1) + ". " + heroList[i]);
                }
                System.out.print("Pilih Hero: ");
                int pilhero = scanner.nextInt();
                if(pilhero >0 && pilhero <= heroList.length){
                    String pilihhero = heroList [pilhero - 1];
                    System.out.println("Anda memilih Hero: " + pilihhero);
                }else {
                    System.out.println("Tidak Valid");
                }
            }else if (pilihRole instanceof Marksman) {
                System.out.println("HERO LIST");
                Marksman marksmanRole = (Marksman) pilihRole;
                String [] heroList = marksmanRole.getHeroList();
                for (int i = 0; i < heroList.length; i++){
                    System.out.println((i + 1) + ". " + heroList[i]);
                }
                System.out.print("Pilih Hero: ");
                int pilhero = scanner.nextInt();
                if(pilhero >0 && pilhero <= heroList.length){
                    String pilihhero = heroList [pilhero - 1];
                    System.out.print("Anda memilih Hero: " + pilihhero);
                }else {
                    System.out.println("Tidak Valid");
                }
            }
            else if(pilihRole instanceof Assassin){
                System.out.println("HERO LIST");
                Assassin assasinRole = (Assassin) pilihRole;
                String [] heroList = assasinRole.getHeroList();
                for (int i = 0; i < heroList.length; i++){
                    System.out.println((i + 1) + ". " + heroList[i]);
                }
                System.out.print("Pilih Hero: ");
                int pilhero = scanner.nextInt();
                if(pilhero >0 && pilhero <= heroList.length){
                    String pilihhero = heroList [pilhero - 1];
                    System.out.println("Anda memilih Hero: " + pilihhero);
                }else {
                    System.out.println("Tidak Valid");
                }
            }
            else if(pilihRole instanceof Support){
                System.out.println("HERO LIST");
                Support supportRole = (Support) pilihRole;
                String [] heroList = supportRole.getHeroList();
                for (int i = 0; i < heroList.length; i++){
                    System.out.println((i + 1) + ". " + heroList[i]);
                }
                System.out.print("Pilih Hero: ");
                int pilhero = scanner.nextInt();
                if(pilhero >0 && pilhero <= heroList.length){
                    String pilihhero = heroList [pilhero - 1];
                    System.out.println("Anda memilih Hero: " + pilihhero);
                }else {
                    System.out.println("Tidak Valid");
                }
            }
            else if(pilihRole instanceof Fighter){
                System.out.println("HERO LIST");
                Fighter fighterRole = (Fighter) pilihRole;
                String [] heroList = fighterRole.getHeroList();
                for (int i = 0; i < heroList.length; i++){
                    System.out.println((i + 1) + ". " + heroList[i]);
                }
                System.out.print("Pilih Hero: ");
                int pilhero = scanner.nextInt();
                if(pilhero >0 && pilhero <= heroList.length){
                    String pilihhero = heroList [pilhero - 1];
                    System.out.print("Anda memilih Hero: " + pilihhero);
                }else {
                    System.out.println("Tidak Valid");
                }
            }
            else if(pilihRole instanceof Mage){
                System.out.println("HERO LIST");
                Mage mageRole = (Mage) pilihRole;
                String [] heroList = mageRole.getHeroList();
                for (int i = 0; i < heroList.length; i++){
                    System.out.println((i + 1) + ". " + heroList[i]);
                }
                System.out.println("Pilih Hero: ");
                int pilhero = scanner.nextInt();
                if(pilhero >0 && pilhero <= heroList.length){
                    String pilihhero = heroList [pilhero - 1];
                    System.out.print("Anda memilih Hero: " + pilihhero);
                }else {
                    System.out.println("Tidak Valid");
                }
            }else{
                System.out.println("tidak ada hero");
            }
        }else {
            System.out.println("Tidak Valid");
        }
    }

}

```
- Pendekatan Algoritma Pemrograman
Metode ini menjalankan program untuk menampilkan Hero yang sesuai dengan role Hero tersebut, player dapat memilih role yang mereka inginkan kemudian menampilkan beberapa hero sesuai role yang dipilih, metode ini menggunakan array list, dimana dalam array list tersebut sudah tersimpan data hero yang berada pada class hero, dan object Scanner untuk menerima input dari user.

# No 2
```java
import java.util.Scanner;
import java.text.DecimalFormat;
import java.util.Random;

class Profile{
    private String nama;
    private int menang;
    private int kalah;
    private int total;
    private double winrate;

    Player player = new Player();
    GamePlay game = new GamePlay();


    Profile(){
        this.nama = nama;
        this.menang = menang;
        this.kalah = kalah;
        this.total = total;
        this.winrate = winrate;
    }

    public String getNama(){
        player.getNama();
        return nama;
    }


    public int getMenang(){
        this.menang = game.getMenang();
        return menang;
    }

    public void show(){
        player.showStatistik();
    }

}
```
1. Buatlah class profile dengan atribut Nama, Menang, Kalah, total dan winrate untuk menampilkan Statistik dari user

```java

class Player {
    private String nama;
    private int level;
    private String password;
    DecimalFormat decimalFormat = new DecimalFormat("#.##");


    public Player(){
        this.nama = nama;
        this.level = level;
        this.password = password;
    }


    public void setPassword(String password){
        this.password = password;
    }
    
    public String getPassword(){
        return password;
    }
    
    public String getNama(){
        return nama;
    }

    public void setNama(String nama){
        this.nama = nama;
    }

    public int getLevel(){
        return level;
    }

    public void setLevel(int level){
        this.level = level;
    }

    public void register(){
        
    }
    

    public void Login(){
        Scanner scanner = new Scanner(System.in);
        int pil;
        GamePlay game = new GamePlay();
        Player player = new Player();
        Profile profile = new Profile();

        System.out.println("\n=== SELAMAT DATANG DI MOBILE LEGENDS ===");
        System.out.println("1. Login");
        System.out.println("2. Register");
        System.out.println("3. Keluar");
        System.out.print("Pilihan : ");
        pil = scanner.nextInt();
        switch(pil){
            case 1:
                //memanggil menu login
                System.out.println("\n=== LOGIN ===");
                System.out.print("Nama : ");
                String nama = scanner.next();
                System.out.print("Password : ");
                String password = scanner.next();
                System.out.print("Masukkan Level: ");
                int lvl = scanner.nextInt();
                this.setNama(nama);
                this.setLevel(lvl);
                if (nama.equals(nama) && password.equals("123")){
                    System.out.println("Login Berhasil");
                    int pil1;

                    do {
                        System.out.println("\n=== SELAMAT DATANG DI MOBILE LEGENDS ===");
                        System.out.println("1. Profile");
                        System.out.println("2. Mode Rank");
                        System.out.println("3. Logout");
                        System.out.print("Pilihan : ");
                        pil1 = scanner.nextInt();

                        switch(pil1){
                            case 1:
                                //memanggil menu profil
                                profile.show();
                                break; 
                            case 2:
                                //memanggil CLI untuk mode rank(pilih role)
                                game.laneHero();
                                game.setMatch();
                                break;
                            case 3:
                                //Logout (kembali ke menu utama)
                                break;
                            default:
                                System.out.println("Tidak Valid");
                                break;
                        }

                    }while(pil1!=3);
                }else{
                    System.out.println("Logiin Gagal!!");
                }
                break;
            case 2:
                //memanggil menu register
                System.out.println("\n=== REGISTER ===");
                break;
            case 3:
                //Keluar
                System.out.println("\nTerima kasih telah bermain!");
                break;
            default:
                System.out.println("Tidak Valid");
                break;
        }while(pil!=3);
}    

void showStatistik(){
    GamePlay game = new GamePlay();

    System.out.println("Username \t\t: " + getNama());
    System.out.println("Level \t\t\t: " + getLevel());
    System.out.println("Total Pertandingan \t: " + game.getTotal());
    System.out.println("Jumlah Kemenangan \t: " + game.getMenang());
    System.out.println("Jumlah Kekalahan \t: " + game.getKalah());
    System.out.println("WINRATE : " + decimalFormat.format(game.getWinrate()) + " %");
}

}
```
2. Buatlah class player dengan atribute nama, password dan level untuk menyimpan data dari user. kemudian buat juga method setter dan getter karena terdapat access modifier private pada atributenya.

```java
 
class GamePlay{
    private int menang;
    private int kalah;
    private int total;
    private double winrate;

    public GamePlay(){
        this.menang = menang;
        this.kalah = kalah;
        this.total = total;
        this.winrate = winrate;
    }

    void addWin(){
        menang++;
        total++;
    }
    void addLose(){
        kalah++;
        total++;
    }

    void setMenang(int menang){
        this.menang = menang;
    }

    public int getMenang(){
        return menang;
    }

    public int getKalah(){
        return kalah;
    }

    public int getTotal(){
        total = menang + kalah;
        return total;
    }
    public double getWinrate(){
        winrate = (double)menang/total*100;
        return winrate;
    }

    public void setMatch(){
        Random random = new Random();
        int randomNomor = random.nextInt(2); //menghasilkan nilai acak dari 0 hingga 1
        if (randomNomor == 0){
            System.out.println("=== VICTORY!!! ===");
            addWin();
        } else {
            System.out.println("=== DEFEAT!!! ===");
            addLose();

        }
        total++;
    }

    void pilihEmblem(){

    }

    void laneHero(){
        Scanner scanner = new Scanner(System.in);
        this.pilihHero();

        System.out.println("\n===== MEMASUKI PERMAINAN =====");
        System.out.println("1. Bergerak ke lane Gold (Disarankan untuk Marksman/Hero yang membutuhkan Scalping diawal )");
        System.out.println("2. Bergerak ke lane Exp(Disarankan untuk Fighter/Hero yang mempunyai damage tinggi diawal)");
        System.out.println("3. Bergerak ke Midlane(Disarankan untuk Mage)");
        System.out.println("4. Bergerak ke arah Jungle(Disarankan untuk Hero Assasin/hero yang memiliki mobilitas tinggi)");
        System.out.print("Pilihan anda : ");
        int pillane = scanner.nextInt();
        switch(pillane){
            case 1:
                this.moveHero();
                break;
            case 2:
                this.moveHero();
                break;
            case 3:
                this.moveHero();
                break;
            case 4:
                this.moveHero();
                break;
            default:
                System.out.println("Error");
                break;
        }
    }

    void moveHero(){
        Scanner scanner = new Scanner(System.in);
        Player player = new Player();
        int pilihmenu;
        do{
            System.out.println("\n1. Kalahkan Minion");
            System.out.println("2. Poking damage Ke musuh");
            System.out.println("3. Kalahkan kepiting");
            System.out.println("4. Bergerak Ke Midlane");
            System.out.println("5. Ambush");
            System.out.println("6. Cutting Lane");
            System.out.println("7. Hancurkan Turret");
            System.out.println("8. Hasil Pertandingan");
            System.out.print("Pilihan: ");
            pilihmenu = scanner.nextInt();

            switch(pilihmenu){
                case 1:
                    System.out.println("test");
                    break;
                case 2:
                    System.out.println("test");
                    break;
                case 3:
                    System.out.println("test");
                    break;
                case 4:
                    System.out.println("test");
                    break;
                case 5:
                    System.out.println("test");
                    break;
                case 6:
                    System.out.println("test");
                    break;
                case 7:
                    System.out.println("test");
                    break;
                case 8:
                    System.out.println();
                    break;
                default:
                    System.out.println("Menu yang anda pilih tidak tersedia");
                    break;
            }
        }while(pilihmenu != 8);
    }

    void pilihHero(){
        Scanner scanner = new Scanner (System.in);
        Hero[] roles = new Hero[6];
        roles[0] = new Assassin ();
        roles[1] = new Fighter();
        roles[2] = new Marksman();
        roles[3] = new Tank();
        roles[4] = new Support();
        roles[5] = new Mage();

        System.out.println("\nROLE LIST");
        for (int i = 0; i < roles.length; i++){
            System.out.println((i+1) + ". " + roles[i].getNama() + " - " + roles[i].getDeskripsi());

        }

        System.out.print("\nMasukkan Role yang anda pilih: ");
        int rolepilihan = scanner.nextInt();
        if(rolepilihan > 0 && rolepilihan <= roles.length){
            Hero pilihRole = roles[rolepilihan - 1];
            if(pilihRole instanceof Tank ){
                System.out.println("HERO LIST");
                Tank tankRole = (Tank) pilihRole;
                String[] heroList = tankRole.getHeroList();
                for (int i = 0; i < heroList.length; i++){
                    System.out.println((i + 1) + ". " + heroList[i]);
                }
                System.out.print("Pilih Hero: ");
                int pilhero = scanner.nextInt();
                if(pilhero >0 && pilhero <= heroList.length){
                    String pilihhero = heroList [pilhero - 1];
                    System.out.println("Anda memilih Hero: " + pilihhero);
                }else {
                    System.out.println("Tidak Valid");
                }
            }else if (pilihRole instanceof Marksman) {
                System.out.println("HERO LIST");
                Marksman marksmanRole = (Marksman) pilihRole;
                String [] heroList = marksmanRole.getHeroList();
                for (int i = 0; i < heroList.length; i++){
                    System.out.println((i + 1) + ". " + heroList[i]);
                }
                System.out.print("Pilih Hero: ");
                int pilhero = scanner.nextInt();
                if(pilhero >0 && pilhero <= heroList.length){
                    String pilihhero = heroList [pilhero - 1];
                    System.out.print("Anda memilih Hero: " + pilihhero);
                }else {
                    System.out.println("Tidak Valid");
                }
            }
            else if(pilihRole instanceof Assassin){
                System.out.println("HERO LIST");
                Assassin assasinRole = (Assassin) pilihRole;
                String [] heroList = assasinRole.getHeroList();
                for (int i = 0; i < heroList.length; i++){
                    System.out.println((i + 1) + ". " + heroList[i]);
                }
                System.out.print("Pilih Hero: ");
                int pilhero = scanner.nextInt();
                if(pilhero >0 && pilhero <= heroList.length){
                    String pilihhero = heroList [pilhero - 1];
                    System.out.println("Anda memilih Hero: " + pilihhero);
                }else {
                    System.out.println("Tidak Valid");
                }
            }
            else if(pilihRole instanceof Support){
                System.out.println("HERO LIST");
                Support supportRole = (Support) pilihRole;
                String [] heroList = supportRole.getHeroList();
                for (int i = 0; i < heroList.length; i++){
                    System.out.println((i + 1) + ". " + heroList[i]);
                }
                System.out.print("Pilih Hero: ");
                int pilhero = scanner.nextInt();
                if(pilhero >0 && pilhero <= heroList.length){
                    String pilihhero = heroList [pilhero - 1];
                    System.out.println("Anda memilih Hero: " + pilihhero);
                }else {
                    System.out.println("Tidak Valid");
                }
            }
            else if(pilihRole instanceof Fighter){
                System.out.println("HERO LIST");
                Fighter fighterRole = (Fighter) pilihRole;
                String [] heroList = fighterRole.getHeroList();
                for (int i = 0; i < heroList.length; i++){
                    System.out.println((i + 1) + ". " + heroList[i]);
                }
                System.out.print("Pilih Hero: ");
                int pilhero = scanner.nextInt();
                if(pilhero >0 && pilhero <= heroList.length){
                    String pilihhero = heroList [pilhero - 1];
                    System.out.print("Anda memilih Hero: " + pilihhero);
                }else {
                    System.out.println("Tidak Valid");
                }
            }
            else if(pilihRole instanceof Mage){
                System.out.println("HERO LIST");
                Mage mageRole = (Mage) pilihRole;
                String [] heroList = mageRole.getHeroList();
                for (int i = 0; i < heroList.length; i++){
                    System.out.println((i + 1) + ". " + heroList[i]);
                }
                System.out.println("Pilih Hero: ");
                int pilhero = scanner.nextInt();
                if(pilhero >0 && pilhero <= heroList.length){
                    String pilihhero = heroList [pilhero - 1];
                    System.out.print("Anda memilih Hero: " + pilihhero);
                }else {
                    System.out.println("Tidak Valid");
                }
            }else{
                System.out.println("tidak ada hero");
            }
        }else {
            System.out.println("Tidak Valid");
        }
    }

}

```
3. Bualah class GamePlay 
```

//class abstrak yang digunakan sebagai kerangka untuk kelas turunannya
abstract class Hero {
    private String nama;
    private String deskripsi;

    public Hero(String nama, String deskripsi) {
        this.nama = nama;
        this.deskripsi = deskripsi;
    }

    public String getNama(){
        return nama;
    }

    public String getDeskripsi(){
        return deskripsi;
    }

}

//Implementasi Inheritance
class Assassin extends Hero {
    private String [] heroList;

    public Assassin() {
        super("Assassin", "Mampu menghasilkan damage yang tinggi dalam waktu singkat, tetapi memiliki HP yang rendah.");
        heroList = new String[] {"Fanny", "Helcurt", "Aamon", "Saber", "Hayabusa", "Ling", "Lancelot"};
    }
    //Implementasi Polimorfisme
    public String[] getHeroList(){
        return heroList;
    }
}

//Implementasi Inheritance
class Mage extends Hero {
    private String [] heroList;

    public Mage() {
        super("Magic", "Hero yangMampu menghasilkan Magic damage yang tinggi");
        heroList = new String[] {"Kagura", "Helcurt", "Aamon", "Saber", "Hayabusa", "Ling", "Lancelot"};
    }
     //Implementasi Polimorfisme
    public String[] getHeroList(){
        return heroList;
    }
}

//Implementasi Inheritance
class Tank extends Hero {
    private String [] heroList; 
    public Tank() {
        super("Tank", "Mampu menahan damage dan menarik perhatian musuh, tetapi memiliki damage yang rendah.");
        heroList = new String[] {"Hylos", "Atlas", "Khufra", "Lolita", "Franco", "Akai", "Tigreal"};
    }
     //Implementasi Polimorfisme
    public String[] getHeroList(){
        return heroList;
    }
}

//Implementasi Inheritance
class Support extends Hero {
    private String [] heroList;
    public Support() {
        super("Support", "Mampu memberikan support pada hero lain dan mengganggu musuh.");
        heroList = new String[] {"Angela", "Floryn", "Mathilda", "Carmila", "Estes"};
    }
     //Implementasi Polimorfisme
    public String[] getHeroList(){
        return heroList;
    }
}

//Implementasi Inheritance
class Marksman extends Hero {
    private String[] heroList;

    public Marksman() {
        super("Marksman", "Mampu menembak dari jarak jauh dan menghasilkan damage yang tinggi, tetapi memiliki HP yang rendah.");
        heroList = new String[] {"Miya", "Claude", "Bruno", "Moskov", "Layla"};
    }
     //Implementasi Polimorfisme
    public String[] getHeroList() {
        return heroList;
    }
    }

    //Implementasi Inheritance
    class Fighter extends Hero {
        private String[] heroList;
        
        public Fighter(){
            super("Fighter", "Hero yang mempunyai darah tebal dan damage cukup tinggi");
            heroList = new String [] {"Lapu-lapu", "Yuzhong", "Chou", "Terizla", "Thamuz"};
        }
         //Implementasi Polimorfisme
        public String [] getHeroList(){
            return heroList;
        }


}



public class revisi {
    public static void main(String[] args) {

        Player player = new Player();
        player.Login();   
    }
}

```

# No 3

```
1. abstract 
    proses menyembunyikan sebuah implementasi dari sebuah objek jadi hanya menampilkan fungsionalitas yang diinginkan saja
2. encapsulation
    untuk menjaga suatu variabel agar tidak bisa diakses secara sembarangan
3. inheritance
    konsep dimana sebuah class dapat mewarisi sifat sifat atribut dari parentclass nya

4. polymorphism
    konsep dimana sebuah class dapat memiliki banyak method yang berbeda beda meskipun namanya sama

```



# No 4
```java
    private String nama;
    private int level;
    private String password;
    DecimalFormat decimalFormat = new DecimalFormat("#.##");


    public Player(){
        this.nama = nama;
        this.level = level;
        this.password = password;
    }


    public void setPassword(String password){
        this.password = password;
    }
    
    public String getPassword(){
        return password;
    }
    
    public String getNama(){
        return nama;
    }

    public void setNama(String nama){
        this.nama = nama;
    }

    public int getLevel(){
        return level;
    }

    public void setLevel(int level){
        this.level = level;
    }
```

# No 5
```java

//class abstract yang digunakan sebagai superclass/parentclass untuk turunannya 
abstract class Hero {
    private String nama;
    private String deskripsi;

    public Hero(String nama, String deskripsi) {
        this.nama = nama;
        this.deskripsi = deskripsi;
    }

    public String getNama(){
        return nama;
    }

    public String getDeskripsi(){
        return deskripsi;
    }

}


```
class Hero merupakan class abstract, diawali dengan code abstract, class abstract tidak dapat diinstansi

# No 6
```java 
class Assassin extends Hero {
    private String [] heroList;

    public Assassin() {
        super("Assassin", "Mampu menghasilkan damage yang tinggi dalam waktu singkat, tetapi memiliki HP yang rendah.");
        heroList = new String[] {"Fanny", "Helcurt", "Aamon", "Saber", "Hayabusa", "Ling", "Lancelot"};
    }

    public String[] getHeroList(){
        return heroList;
    }
}
class Mage extends Hero {
    private String [] heroList;

    public Mage() {
        super("Magic", "Hero yangMampu menghasilkan Magic damage yang tinggi");
        heroList = new String[] {"Kagura", "Helcurt", "Aamon", "Saber", "Hayabusa", "Ling", "Lancelot"};
    }

    public String[] getHeroList(){
        return heroList;
    }
}

class Tank extends Hero {
    private String [] heroList; 
    public Tank() {
        super("Tank", "Mampu menahan damage dan menarik perhatian musuh, tetapi memiliki damage yang rendah.");
        heroList = new String[] {"Hylos", "Atlas", "Khufra", "Lolita", "Franco", "Akai", "Tigreal"};
    }

    public String[] getHeroList(){
        return heroList;
    }
}

class Support extends Hero {
    private String [] heroList;
    public Support() {
        super("Support", "Mampu memberikan support pada hero lain dan mengganggu musuh.");
        heroList = new String[] {"Angela", "Floryn", "Mathilda", "Carmila", "Estes"};
    }
    public String[] getHeroList(){
        return heroList;
    }
}

class Marksman extends Hero {
    private String[] heroList;

    public Marksman() {
        super("Marksman", "Mampu menembak dari jarak jauh dan menghasilkan damage yang tinggi, tetapi memiliki HP yang rendah.");
        heroList = new String[] {"Miya", "Claude", "Bruno", "Moskov", "Layla"};
    }
    public String[] getHeroList() {
        return heroList;
    }
    }

    class Fighter extends Hero {
        private String[] heroList;
        
        public Fighter(){
            super("Fighter", "Hero yang mempunyai darah tebal dan damage cukup tinggi");
            heroList = new String [] {"Lapu-lapu", "Yuzhong", "Chou", "Terizla", "Thamuz"};
        }

        public String [] getHeroList(){
            return heroList;
        }


}

```


# No 7

## Proses Bisnis Mobile Legends

Login/Register: Pengguna dapat masuk keakun mereka dengan memasukkan username dan password, jika pengguna belum mendaftar maka harus mendaftarkan akun dahulu

profile: user dapat melihat profile yang didalamnya terdapat statistik hasil pertandingan seperti kemenangan, kekalahan, jumlah pertandingan dan winrate.

Mode permainan : Pengguna dapat memilih Pertandingan yang ingin dimainkan seperti classic, Rank dan Brawl

Pertandingan: User dapat memainkan pertandingan menggunakan Hero yang telah dipilih, dimana didalam permainan user dapat mengekplorasi map, mengontrol hero, mengalahkan jungle dan mengahncurkan tower untuk memenangkan pertandingan

Pemilihan Hero: Pengguna dapat memilih Hero sesuai dengan keinginan untuk dimainkan

logout: user bisa keluar dari game 

# No 8

## Class Diagram
--- 
Mobile Legends
---
```mermaid
classDiagram
    GamePlay <|-- Hero
    GamePlay: +lane()
    GamePlay: +move()
    GamePlay: +pilihHero()
    GamePlay: +pilihRole()
    Player --|> GamePlay
    Player : -String nama
    Player : -String password
    Player: +register()
    Player: +login()
    Profile <|-- Player
    Profile : -String Username
    Profile : -int menang
    Profile : -int kalah
    Profile : -int total
    Profile : -double winrate
    Profile : +statistik()
    Hero <|-- Assasin
    Hero <|-- Tank
    Hero <|-- Mage
    Hero <|-- Marksman
    Hero <|-- Fighter
    Hero <|-- Support
    Hero : +String nama
    Hero : +String deskripsi
    Hero: +attack()
    Hero: +health()
    Hero: +deffensePower()


    class Assasin{
      - String nama
      - String deskripsi 
      
      +attack()
      +deffensePower()
    }
     class Support{
      - String nama
      - String deskripsi 
      
      +attack()
      +deffensePower()

    }
     class Marksman{
      - String nama
      - String deskripsi 
      
      +attack()
      +deffensePower()

    }
     class Mage{
      - String nama
      - String deskripsi 
      
      +attack()
      +deffensePower()

    }
     class Tank{
      - String nama
      - String deskripsi 
      
      +attack()
      +deffensePower()

    }

    class Fighter{
      - String nama
      - String deskripsi 
      
      +attack()
      +deffensePower()

    }
```





## Use Case Table
NO | Use Case | Nilai Prioritas
--- | --- | ---
1 | User dapat melihat menu profile | 100
2 | User dapat membuka shop | 80
3 | User dapat melihat leaderboard | 80
4 | User dapat memilih mode permainan | 100
5 | User dapat melihat events | 70
6 | User dapat mengambil hadiah harian | 60
7 | User dapat membuka menu persiapan | 90
8 | User dapat membuka backpack/inventory | 75
9 | User dapat mengirim pesan ke global |70
10 | User dapat topup diamonds | 80
11 | User dapat melihat Pesan |80
12 | User dapat melihat Player lain yang bermain | 60
13 | User dapat membuka pengaturan | 90
14 | User dapat melihat friendlist | 70
15 | User dapat melihat menu hero | 70
16 | User dapat mengatur border | 70
17 | User dapat mengubah nama | 70
18 | User dapat melihat medali | 70
19 | User dapat mengatur avatar | 70
20 | User dapat menyalin nama | 70
21 | User dapat membuka menu album | 60
22 | User dapat memasukkan foto | 50
23 | User dapat memberikan like | 50
24 | User dapat menambahkan bio | 60
25 | User dapat memberikan komentar | 50
26 | User dapat melihat riwayat pertandingan | 80
27 | User dapat membuka menu Battlefield | 90
28 | User dapat melihat statistik season saat ini | 90
29 | User dapat melihat statistik semua season | 90
30 | User dapat melihat statistik sesuai mode permainan | 90
31 | User dapat membuka menu favorit | 90
32 | User dapat melihat hero favorit | 90
33 | User dapat melihat replay | 60
34 | User dapat membuka pengaturan akun | 90
35 | User dapat membeli Hero | 90
36 | User dapat memperoleh fragment | 90
37 | User dapat membeli Skin | 90
38 | User dapat membeli battle emote | 90
39 | User dapat membeli emblem fragmen | 90
40 | User dapat membeli Starlight | 90
41 | User dapat membeli rename card | 90
42 | User dapat memilih Hero | 100
43 | User dapat memilih emblem | 90
44 | User dapat memilih spell | 90
45 | User dapat memilih skin | 80
46 | User dapat recall | 70
47 | User dapat membuka skill hero | 100
48 | User dapat menyesuaikan build item | 60
49 | User dapat mengupgrade skill | 90
50 | User dapat menggerakkan hero | 100
51 | User dapat membeli equipment | 90
52 | User dapat membuka minimap | 70
53 | Hero dapat meningkatkan level | 90
54 | User dapat membuka pengaturan | 80
55 | User dapat menyerang monster jungle | 90
56 | User dapat mengalahkan turtle | 60
57 | User dapat mengalahkan lord | 70
58 | User dapat menyerang hero yang berada ditim musuh | 80
59 | User dapat menghancurkan turret | 100
60 | User dapat mengirim pesan didalam permainan | 50
61 | User dapat melihat statistuk pertandingan | 80
62 | User dapat menjelajahi map | 90
63 | User dapat menyesuaikan grafik | 80
64 | User dapat melihat hasil pertandingan | 70
65 | User dapar melihat top rank point | 70
66 | User dapat melihat top rank hero | 70
67 | User dapat melihat achievement point | 70
68 | User dapat melihat popularitas | 70
69 | User dapat menyesuaikan emblem | 70
70 | User dapat mengupgrade emblem | 70
71 | User dapat membuka emblem | 80
72 | User dapat melihat battle spells | 70
73 | User dapat mengatur build hero | 70
74 | User dapat melihat informasi tentang equipment | 70
75 | User dapat mengatur efek dalam ingame | 60
76 | User dapat mengatur quick chat | 50
77 | User dapat mengatur posisi tombol ingame | 75
78 | User dapat melihat item yang diperoleh | 60
79 | User dapat menggunakan item | 40
80 | User dapat menggunakan trial card | 40
81 | User dapat mengatur item sesuai waktu diperoleh |40
82 | User dapat mengatur penargetan | 85
83 | User dapat mengatur prioritas penargetan | 80
84 | User dapat mengatur kontrol lanjutan | 70
85 | User dapat mengatur suara | 90
86 | User dapat mengatur jaringan | 70
87 | User dapat mengubah bahasa game | 70
88 | User dapat mengirim pesan ke User lain | 70
89 | User dapat mengundang teman ke lobby | 80
90 | User dapat menambah teman | 80
91 | User dapat membuat grup | 60
92 | User dapat membuat squad | 60
93 | User dapat masuk dalam grup | 50
94 | User dapat masuk dalam squad | 60
95 | User dapat menggunakan menu pelayanan | 90
96 | User dapat melihat chat global | 60
97 | User dapat menerima pesan | 60




# No 9

# No 10


